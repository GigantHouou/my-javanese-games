extends MarginContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_select"):
		get_tree().change_scene(str("res://Scenes/Level.tscn"))
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
