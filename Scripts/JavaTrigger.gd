extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (String) var sceneName = "Level"

func _on_Area2D_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Pemain":
		if current_scene == sceneName:
			global.lives -= 1
		if (global.lives == 0):
			get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
			global.lives += 1
		else:
			get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))


