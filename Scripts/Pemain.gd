extends KinematicBody2D

export (int) var speed = 600
export (int) var jump_speed = -700
export (int) var GRAVITY = 1200
var has_double_jump = false

const UP = Vector2(0, -1)

var velocity = Vector2()

func get_input():
	velocity.x = 0
	if is_on_floor():
		has_double_jump = false
	if Input.is_action_just_pressed('ui_up'):
		if is_on_floor():
			velocity.y = jump_speed
		elif !has_double_jump:
			velocity.y = jump_speed
			has_double_jump = true
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

