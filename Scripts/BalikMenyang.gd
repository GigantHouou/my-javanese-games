extends LinkButton

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(String) var scene_to_load
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _on_BalikMenyang_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))


func _on_LinkButton_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
