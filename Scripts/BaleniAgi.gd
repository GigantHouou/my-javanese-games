extends LinkButton

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# Called when the node enters the scene tree for the first time.
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
export(String) var scene_to_load
func _on_BaleniAgi_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))


func _on_LinkButton2_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
